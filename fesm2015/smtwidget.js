import { Injectable, Component, ElementRef, Input, NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @return {?}
 */
function _window() {
    // return the global native browser window object
    return window;
}
class WindowRef {
    /**
     * @return {?}
     */
    get nativeWindow() {
        return _window();
    }
}
WindowRef.decorators = [
    { type: Injectable }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SmtWidgetComponent {
    /**
     * @param {?} el
     * @param {?} win
     */
    constructor(el, win) {
        this.el = el;
        this.win = win;
        this.initializeWidget = this.initializeWidget.bind(this);
        this.setProps = this.setProps.bind(this);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.mEl) {
            this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
        }
        document.addEventListener('widgetLoaded', this.initializeWidget, true);
        if (!this.win.nativeWindow.SmtBoot) {
            /** @type {?} */
            var $script = document.createElement('script');
            //$script.src = '/dist/boot.min.js';
            $script.src = 'https://cdn.smartvel.com/scripts/boot.min.js';
            document.body.appendChild($script);
        }
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        if (!this.mEl) {
            this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
        }
        this.setProps(this.props);
        if (!!this.win.nativeWindow.SMTWidget) {
            this.win.nativeWindow.SMTWidget.updateDataset(this.props);
            /** @type {?} */
            var event = new CustomEvent('SMTEngage');
            document.dispatchEvent(event);
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.win.nativeWindow.SmtBoot.initialized = false;
    }
    /**
     * @private
     * @return {?}
     */
    initializeWidget() {
        if (!!this.win.nativeWindow.SmtBoot &&
            !this.win.nativeWindow.SmtBoot.initialized) {
            this.win.nativeWindow.SmtBoot.boot();
        }
    }
    /**
     * @private
     * @param {?} p
     * @return {?}
     */
    setProps(p) {
        /** @type {?} */
        const self = this;
        if (!!p) {
            /** @type {?} */
            var ks = Object.keys(p);
            if (!!this.mEl.dataset) {
                ks.forEach((/**
                 * @param {?} el
                 * @return {?}
                 */
                (el) => {
                    self.mEl.dataset[el] = p[el];
                }));
            }
        }
    }
}
SmtWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-smtwidget',
                template: '<SmartvelComponent></SmartvelComponent>',
                providers: [WindowRef]
            }] }
];
/** @nocollapse */
SmtWidgetComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: WindowRef }
];
SmtWidgetComponent.propDecorators = {
    props: [{ type: Input }]
};

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class SmtWidgetModule {
}
SmtWidgetModule.decorators = [
    { type: NgModule, args: [{
                declarations: [SmtWidgetComponent],
                imports: [],
                exports: [SmtWidgetComponent],
                schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
            },] }
];

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { SmtWidgetComponent, SmtWidgetModule, WindowRef };
//# sourceMappingURL=smtwidget.js.map
