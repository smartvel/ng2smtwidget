/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of smt-widget
 */
export { WindowRef } from './lib/smt-widget.service';
export { SmtWidgetComponent } from './lib/smt-widget.component';
export { SmtWidgetModule } from './lib/smt-widget.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3NtdHdpZGdldC8iLCJzb3VyY2VzIjpbInB1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7OztBQUlBLDBCQUFjLDBCQUEwQixDQUFDO0FBQ3pDLG1DQUFjLDRCQUE0QixDQUFDO0FBQzNDLGdDQUFjLHlCQUF5QixDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLypcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBzbXQtd2lkZ2V0XG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvc210LXdpZGdldC5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NtdC13aWRnZXQuY29tcG9uZW50JztcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NtdC13aWRnZXQubW9kdWxlJztcbiJdfQ==