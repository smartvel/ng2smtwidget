/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ElementRef } from '@angular/core';
import { WindowRef } from './smt-widget.service';
var SmtWidgetComponent = /** @class */ (function () {
    function SmtWidgetComponent(el, win) {
        this.el = el;
        this.win = win;
        this.initializeWidget = this.initializeWidget.bind(this);
        this.setProps = this.setProps.bind(this);
    }
    /**
     * @return {?}
     */
    SmtWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (!this.mEl) {
            this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
        }
        document.addEventListener('widgetLoaded', this.initializeWidget, true);
        if (!this.win.nativeWindow.SmtBoot) {
            /** @type {?} */
            var $script = document.createElement('script');
            //$script.src = '/dist/boot.min.js';
            $script.src = 'https://cdn.smartvel.com/scripts/boot.min.js';
            document.body.appendChild($script);
        }
    };
    /**
     * @return {?}
     */
    SmtWidgetComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        if (!this.mEl) {
            this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
        }
        this.setProps(this.props);
        if (!!this.win.nativeWindow.SMTWidget) {
            this.win.nativeWindow.SMTWidget.updateDataset(this.props);
            /** @type {?} */
            var event = new CustomEvent('SMTEngage');
            document.dispatchEvent(event);
        }
    };
    /**
     * @return {?}
     */
    SmtWidgetComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.win.nativeWindow.SmtBoot.initialized = false;
    };
    /**
     * @private
     * @return {?}
     */
    SmtWidgetComponent.prototype.initializeWidget = /**
     * @private
     * @return {?}
     */
    function () {
        if (!!this.win.nativeWindow.SmtBoot &&
            !this.win.nativeWindow.SmtBoot.initialized) {
            this.win.nativeWindow.SmtBoot.boot();
        }
    };
    /**
     * @private
     * @param {?} p
     * @return {?}
     */
    SmtWidgetComponent.prototype.setProps = /**
     * @private
     * @param {?} p
     * @return {?}
     */
    function (p) {
        /** @type {?} */
        var self = this;
        if (!!p) {
            /** @type {?} */
            var ks = Object.keys(p);
            if (!!this.mEl.dataset) {
                ks.forEach((/**
                 * @param {?} el
                 * @return {?}
                 */
                function (el) {
                    self.mEl.dataset[el] = p[el];
                }));
            }
        }
    };
    SmtWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ngx-smtwidget',
                    template: '<SmartvelComponent></SmartvelComponent>',
                    providers: [WindowRef]
                }] }
    ];
    /** @nocollapse */
    SmtWidgetComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: WindowRef }
    ]; };
    SmtWidgetComponent.propDecorators = {
        props: [{ type: Input }]
    };
    return SmtWidgetComponent;
}());
export { SmtWidgetComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SmtWidgetComponent.prototype.mEl;
    /** @type {?} */
    SmtWidgetComponent.prototype.props;
    /**
     * @type {?}
     * @private
     */
    SmtWidgetComponent.prototype.el;
    /**
     * @type {?}
     * @private
     */
    SmtWidgetComponent.prototype.win;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic210LXdpZGdldC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zbXR3aWRnZXQvIiwic291cmNlcyI6WyJsaWIvc210LXdpZGdldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUF3QixVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0YsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRWpEO0lBVUUsNEJBQ1UsRUFBYSxFQUNiLEdBQWE7UUFEYixPQUFFLEdBQUYsRUFBRSxDQUFXO1FBQ2IsUUFBRyxHQUFILEdBQUcsQ0FBVTtRQUVyQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUN4RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO0lBQzFDLENBQUM7Ozs7SUFFRCxxQ0FBUTs7O0lBQVI7UUFDRSxJQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBQztZQUNYLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtTQUM5RTtRQUNELFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixFQUFFLElBQUksQ0FBQyxDQUFBO1FBQ3RFLElBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUM7O2dCQUM1QixPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7WUFDOUMsb0NBQW9DO1lBQ3BDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsOENBQThDLENBQUM7WUFDN0QsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDcEM7SUFDSCxDQUFDOzs7O0lBRUQsd0NBQVc7OztJQUFYO1FBQ0UsSUFBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUM7WUFDWCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDOUU7UUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQTtRQUN6QixJQUNFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxTQUFTLEVBQ2xDO1lBQ0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7O2dCQUN0RCxLQUFLLEdBQUcsSUFBSSxXQUFXLENBQUMsV0FBVyxDQUFDO1lBQ3hDLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDL0I7SUFDSCxDQUFDOzs7O0lBRUQsd0NBQVc7OztJQUFYO1FBQ0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUE7SUFDbkQsQ0FBQzs7Ozs7SUFFTyw2Q0FBZ0I7Ozs7SUFBeEI7UUFDRSxJQUNFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPO1lBQy9CLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFDM0M7WUFDQyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDdEM7SUFDSCxDQUFDOzs7Ozs7SUFFTyxxQ0FBUTs7Ozs7SUFBaEIsVUFBaUIsQ0FBSTs7WUFDYixJQUFJLEdBQUcsSUFBSTtRQUNqQixJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUM7O2dCQUNGLEVBQUUsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN2QixJQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBQztnQkFDcEIsRUFBRSxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBQyxFQUFFO29CQUNaLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQTtnQkFDOUIsQ0FBQyxFQUFDLENBQUE7YUFDSDtTQUNGO0lBQ0gsQ0FBQzs7Z0JBcEVGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsUUFBUSxFQUFFLHlDQUF5QztvQkFFbkQsU0FBUyxFQUFFLENBQUUsU0FBUyxDQUFFO2lCQUN6Qjs7OztnQkFSd0QsVUFBVTtnQkFDMUQsU0FBUzs7O3dCQVdmLEtBQUs7O0lBNERSLHlCQUFDO0NBQUEsQUFyRUQsSUFxRUM7U0EvRFksa0JBQWtCOzs7Ozs7SUFFN0IsaUNBQWdCOztJQUNoQixtQ0FBeUI7Ozs7O0lBRXZCLGdDQUFxQjs7Ozs7SUFDckIsaUNBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0LCBPbkNoYW5nZXMsIE9uRGVzdHJveSwgRWxlbWVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgV2luZG93UmVmIH0gZnJvbSAnLi9zbXQtd2lkZ2V0LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZ3gtc210d2lkZ2V0JyxcbiAgdGVtcGxhdGU6ICc8U21hcnR2ZWxDb21wb25lbnQ+PC9TbWFydHZlbENvbXBvbmVudD4nLFxuICBzdHlsZXM6IFtdLFxuICBwcm92aWRlcnM6IFsgV2luZG93UmVmIF1cbn0pXG5leHBvcnQgY2xhc3MgU210V2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkNoYW5nZXMsIE9uRGVzdHJveSB7XG5cbiAgcHJpdmF0ZSBtRWw6IGFueVxuICBASW5wdXQoKSBwdWJsaWMgcHJvcHM6IHt9XG4gIGNvbnN0cnVjdG9yKFxuICAgIHByaXZhdGUgZWw6RWxlbWVudFJlZixcbiAgICBwcml2YXRlIHdpbjpXaW5kb3dSZWZcbiAgKSB7XG4gICAgdGhpcy5pbml0aWFsaXplV2lkZ2V0ID0gdGhpcy5pbml0aWFsaXplV2lkZ2V0LmJpbmQodGhpcylcbiAgICB0aGlzLnNldFByb3BzID0gdGhpcy5zZXRQcm9wcy5iaW5kKHRoaXMpXG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgICBpZighdGhpcy5tRWwpe1xuICAgICAgdGhpcy5tRWwgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoJ1NtYXJ0dmVsQ29tcG9uZW50JylbMF1cbiAgICB9XG4gICAgZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignd2lkZ2V0TG9hZGVkJywgdGhpcy5pbml0aWFsaXplV2lkZ2V0LCB0cnVlKVxuICAgIGlmKCF0aGlzLndpbi5uYXRpdmVXaW5kb3cuU210Qm9vdCl7XG4gICAgICB2YXIgJHNjcmlwdCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ3NjcmlwdCcpO1xuICAgICAgLy8kc2NyaXB0LnNyYyA9ICcvZGlzdC9ib290Lm1pbi5qcyc7XG4gICAgICAkc2NyaXB0LnNyYyA9ICdodHRwczovL2Nkbi5zbWFydHZlbC5jb20vc2NyaXB0cy9ib290Lm1pbi5qcyc7XG4gICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKCRzY3JpcHQpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25DaGFuZ2VzKCkge1xuICAgIGlmKCF0aGlzLm1FbCl7XG4gICAgICB0aGlzLm1FbCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnU21hcnR2ZWxDb21wb25lbnQnKVswXVxuICAgIH1cbiAgICB0aGlzLnNldFByb3BzKHRoaXMucHJvcHMpXG4gICAgaWYoXG4gICAgICAhIXRoaXMud2luLm5hdGl2ZVdpbmRvdy5TTVRXaWRnZXRcbiAgICApe1xuICAgICAgdGhpcy53aW4ubmF0aXZlV2luZG93LlNNVFdpZGdldC51cGRhdGVEYXRhc2V0KHRoaXMucHJvcHMpO1xuICAgICAgdmFyIGV2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KCdTTVRFbmdhZ2UnKTtcbiAgICAgIGRvY3VtZW50LmRpc3BhdGNoRXZlbnQoZXZlbnQpO1xuICAgIH1cbiAgfVxuXG4gIG5nT25EZXN0cm95KCkge1xuICAgIHRoaXMud2luLm5hdGl2ZVdpbmRvdy5TbXRCb290LmluaXRpYWxpemVkID0gZmFsc2VcbiAgfVxuXG4gIHByaXZhdGUgaW5pdGlhbGl6ZVdpZGdldCgpe1xuICAgIGlmKFxuICAgICAgISF0aGlzLndpbi5uYXRpdmVXaW5kb3cuU210Qm9vdCAmJlxuICAgICAgIXRoaXMud2luLm5hdGl2ZVdpbmRvdy5TbXRCb290LmluaXRpYWxpemVkXG4gICAgKXtcbiAgICAgIHRoaXMud2luLm5hdGl2ZVdpbmRvdy5TbXRCb290LmJvb3QoKTtcbiAgICB9XG4gIH1cblxuICBwcml2YXRlIHNldFByb3BzKHA6e30pe1xuICAgIGNvbnN0IHNlbGYgPSB0aGlzXG4gICAgaWYgKCEhcCl7XG4gICAgICB2YXIga3MgPSBPYmplY3Qua2V5cyhwKVxuICAgICAgaWYoISF0aGlzLm1FbC5kYXRhc2V0KXtcbiAgICAgICAga3MuZm9yRWFjaCgoZWwpID0+IHtcbiAgICAgICAgICBzZWxmLm1FbC5kYXRhc2V0W2VsXSA9IHBbZWxdXG4gICAgICAgIH0pXG4gICAgICB9XG4gICAgfVxuICB9XG59XG4iXX0=