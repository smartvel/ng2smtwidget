/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { SmtWidgetComponent } from './smt-widget.component';
var SmtWidgetModule = /** @class */ (function () {
    function SmtWidgetModule() {
    }
    SmtWidgetModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [SmtWidgetComponent],
                    imports: [],
                    exports: [SmtWidgetComponent],
                    schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
                },] }
    ];
    return SmtWidgetModule;
}());
export { SmtWidgetModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic210LXdpZGdldC5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zbXR3aWRnZXQvIiwic291cmNlcyI6WyJsaWIvc210LXdpZGdldC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxRQUFRLEVBQUUsc0JBQXNCLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFFNUQ7SUFBQTtJQU0rQixDQUFDOztnQkFOL0IsUUFBUSxTQUFDO29CQUNSLFlBQVksRUFBRSxDQUFDLGtCQUFrQixDQUFDO29CQUNsQyxPQUFPLEVBQUUsRUFBRTtvQkFDWCxPQUFPLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztvQkFDN0IsT0FBTyxFQUFFLENBQUUsc0JBQXNCLEVBQUUsZ0JBQWdCLENBQUU7aUJBQ3REOztJQUM4QixzQkFBQztDQUFBLEFBTmhDLElBTWdDO1NBQW5CLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSwgQ1VTVE9NX0VMRU1FTlRTX1NDSEVNQSwgTk9fRVJST1JTX1NDSEVNQSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU210V2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi9zbXQtd2lkZ2V0LmNvbXBvbmVudCc7XG5cbkBOZ01vZHVsZSh7XG4gIGRlY2xhcmF0aW9uczogW1NtdFdpZGdldENvbXBvbmVudF0sXG4gIGltcG9ydHM6IFtdLFxuICBleHBvcnRzOiBbU210V2lkZ2V0Q29tcG9uZW50XSxcbiAgc2NoZW1hczogWyBDVVNUT01fRUxFTUVOVFNfU0NIRU1BLCBOT19FUlJPUlNfU0NIRU1BIF0sXG59KVxuZXhwb3J0IGNsYXNzIFNtdFdpZGdldE1vZHVsZSB7IH1cbiJdfQ==