import { OnInit, OnChanges, OnDestroy, ElementRef } from '@angular/core';
import { WindowRef } from './smt-widget.service';
export declare class SmtWidgetComponent implements OnInit, OnChanges, OnDestroy {
    private el;
    private win;
    private mEl;
    props: {};
    constructor(el: ElementRef, win: WindowRef);
    ngOnInit(): void;
    ngOnChanges(): void;
    ngOnDestroy(): void;
    private initializeWidget;
    private setProps;
}
