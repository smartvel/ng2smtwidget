/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Injectable } from '@angular/core';
/**
 * @return {?}
 */
function _window() {
    // return the global native browser window object
    return window;
}
export class WindowRef {
    /**
     * @return {?}
     */
    get nativeWindow() {
        return _window();
    }
}
WindowRef.decorators = [
    { type: Injectable }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic210LXdpZGdldC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vc210d2lkZ2V0LyIsInNvdXJjZXMiOlsibGliL3NtdC13aWRnZXQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7OztBQUUzQyxTQUFTLE9BQU87SUFDYixpREFBaUQ7SUFDakQsT0FBTyxNQUFNLENBQUM7QUFDakIsQ0FBQztBQUdELE1BQU0sT0FBTyxTQUFTOzs7O0lBQ25CLElBQUksWUFBWTtRQUNiLE9BQU8sT0FBTyxFQUFFLENBQUM7SUFDcEIsQ0FBQzs7O1lBSkgsVUFBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuZnVuY3Rpb24gX3dpbmRvdygpIDogYW55IHtcbiAgIC8vIHJldHVybiB0aGUgZ2xvYmFsIG5hdGl2ZSBicm93c2VyIHdpbmRvdyBvYmplY3RcbiAgIHJldHVybiB3aW5kb3c7XG59XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBXaW5kb3dSZWYge1xuICAgZ2V0IG5hdGl2ZVdpbmRvdygpIDogYW55IHtcbiAgICAgIHJldHVybiBfd2luZG93KCk7XG4gICB9XG59XG4iXX0=