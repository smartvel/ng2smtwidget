/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input, ElementRef } from '@angular/core';
import { WindowRef } from './smt-widget.service';
export class SmtWidgetComponent {
    /**
     * @param {?} el
     * @param {?} win
     */
    constructor(el, win) {
        this.el = el;
        this.win = win;
        this.initializeWidget = this.initializeWidget.bind(this);
        this.setProps = this.setProps.bind(this);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.mEl) {
            this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
        }
        document.addEventListener('widgetLoaded', this.initializeWidget, true);
        if (!this.win.nativeWindow.SmtBoot) {
            /** @type {?} */
            var $script = document.createElement('script');
            //$script.src = '/dist/boot.min.js';
            $script.src = 'https://cdn.smartvel.com/scripts/boot.min.js';
            document.body.appendChild($script);
        }
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        if (!this.mEl) {
            this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
        }
        this.setProps(this.props);
        if (!!this.win.nativeWindow.SMTWidget) {
            this.win.nativeWindow.SMTWidget.updateDataset(this.props);
            /** @type {?} */
            var event = new CustomEvent('SMTEngage');
            document.dispatchEvent(event);
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.win.nativeWindow.SmtBoot.initialized = false;
    }
    /**
     * @private
     * @return {?}
     */
    initializeWidget() {
        if (!!this.win.nativeWindow.SmtBoot &&
            !this.win.nativeWindow.SmtBoot.initialized) {
            this.win.nativeWindow.SmtBoot.boot();
        }
    }
    /**
     * @private
     * @param {?} p
     * @return {?}
     */
    setProps(p) {
        /** @type {?} */
        const self = this;
        if (!!p) {
            /** @type {?} */
            var ks = Object.keys(p);
            if (!!this.mEl.dataset) {
                ks.forEach((/**
                 * @param {?} el
                 * @return {?}
                 */
                (el) => {
                    self.mEl.dataset[el] = p[el];
                }));
            }
        }
    }
}
SmtWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'ngx-smtwidget',
                template: '<SmartvelComponent></SmartvelComponent>',
                providers: [WindowRef]
            }] }
];
/** @nocollapse */
SmtWidgetComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: WindowRef }
];
SmtWidgetComponent.propDecorators = {
    props: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    SmtWidgetComponent.prototype.mEl;
    /** @type {?} */
    SmtWidgetComponent.prototype.props;
    /**
     * @type {?}
     * @private
     */
    SmtWidgetComponent.prototype.el;
    /**
     * @type {?}
     * @private
     */
    SmtWidgetComponent.prototype.win;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic210LXdpZGdldC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9zbXR3aWRnZXQvIiwic291cmNlcyI6WyJsaWIvc210LXdpZGdldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQUFBLE9BQU8sRUFBRSxTQUFTLEVBQVUsS0FBSyxFQUF3QixVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0YsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBUWpELE1BQU0sT0FBTyxrQkFBa0I7Ozs7O0lBSTdCLFlBQ1UsRUFBYSxFQUNiLEdBQWE7UUFEYixPQUFFLEdBQUYsRUFBRSxDQUFXO1FBQ2IsUUFBRyxHQUFILEdBQUcsQ0FBVTtRQUVyQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQTtRQUN4RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFBO0lBQzFDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ04sSUFBRyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUM7WUFDWCxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLG9CQUFvQixDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUE7U0FDOUU7UUFDRCxRQUFRLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQTtRQUN0RSxJQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxFQUFDOztnQkFDNUIsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDO1lBQzlDLG9DQUFvQztZQUNwQyxPQUFPLENBQUMsR0FBRyxHQUFHLDhDQUE4QyxDQUFDO1lBQzdELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3BDO0lBQ0gsQ0FBQzs7OztJQUVELFdBQVc7UUFDVCxJQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBQztZQUNYLElBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsb0JBQW9CLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQTtTQUM5RTtRQUNELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQ3pCLElBQ0UsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFDbEM7WUFDQyxJQUFJLENBQUMsR0FBRyxDQUFDLFlBQVksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Z0JBQ3RELEtBQUssR0FBRyxJQUFJLFdBQVcsQ0FBQyxXQUFXLENBQUM7WUFDeEMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMvQjtJQUNILENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1QsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUE7SUFDbkQsQ0FBQzs7Ozs7SUFFTyxnQkFBZ0I7UUFDdEIsSUFDRSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTztZQUMvQixDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQzNDO1lBQ0MsSUFBSSxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO1NBQ3RDO0lBQ0gsQ0FBQzs7Ozs7O0lBRU8sUUFBUSxDQUFDLENBQUk7O2NBQ2IsSUFBSSxHQUFHLElBQUk7UUFDakIsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFDOztnQkFDRixFQUFFLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdkIsSUFBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUM7Z0JBQ3BCLEVBQUUsQ0FBQyxPQUFPOzs7O2dCQUFDLENBQUMsRUFBRSxFQUFFLEVBQUU7b0JBQ2hCLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQTtnQkFDOUIsQ0FBQyxFQUFDLENBQUE7YUFDSDtTQUNGO0lBQ0gsQ0FBQzs7O1lBcEVGLFNBQVMsU0FBQztnQkFDVCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsUUFBUSxFQUFFLHlDQUF5QztnQkFFbkQsU0FBUyxFQUFFLENBQUUsU0FBUyxDQUFFO2FBQ3pCOzs7O1lBUndELFVBQVU7WUFDMUQsU0FBUzs7O29CQVdmLEtBQUs7Ozs7Ozs7SUFETixpQ0FBZ0I7O0lBQ2hCLG1DQUF5Qjs7Ozs7SUFFdkIsZ0NBQXFCOzs7OztJQUNyQixpQ0FBcUIiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgSW5wdXQsIE9uQ2hhbmdlcywgT25EZXN0cm95LCBFbGVtZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBXaW5kb3dSZWYgfSBmcm9tICcuL3NtdC13aWRnZXQuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25neC1zbXR3aWRnZXQnLFxuICB0ZW1wbGF0ZTogJzxTbWFydHZlbENvbXBvbmVudD48L1NtYXJ0dmVsQ29tcG9uZW50PicsXG4gIHN0eWxlczogW10sXG4gIHByb3ZpZGVyczogWyBXaW5kb3dSZWYgXVxufSlcbmV4cG9ydCBjbGFzcyBTbXRXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uQ2hhbmdlcywgT25EZXN0cm95IHtcblxuICBwcml2YXRlIG1FbDogYW55XG4gIEBJbnB1dCgpIHB1YmxpYyBwcm9wczoge31cbiAgY29uc3RydWN0b3IoXG4gICAgcHJpdmF0ZSBlbDpFbGVtZW50UmVmLFxuICAgIHByaXZhdGUgd2luOldpbmRvd1JlZlxuICApIHtcbiAgICB0aGlzLmluaXRpYWxpemVXaWRnZXQgPSB0aGlzLmluaXRpYWxpemVXaWRnZXQuYmluZCh0aGlzKVxuICAgIHRoaXMuc2V0UHJvcHMgPSB0aGlzLnNldFByb3BzLmJpbmQodGhpcylcbiAgfVxuXG4gIG5nT25Jbml0KCkge1xuICAgIGlmKCF0aGlzLm1FbCl7XG4gICAgICB0aGlzLm1FbCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnU21hcnR2ZWxDb21wb25lbnQnKVswXVxuICAgIH1cbiAgICBkb2N1bWVudC5hZGRFdmVudExpc3RlbmVyKCd3aWRnZXRMb2FkZWQnLCB0aGlzLmluaXRpYWxpemVXaWRnZXQsIHRydWUpXG4gICAgaWYoIXRoaXMud2luLm5hdGl2ZVdpbmRvdy5TbXRCb290KXtcbiAgICAgIHZhciAkc2NyaXB0ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2NyaXB0Jyk7XG4gICAgICAvLyRzY3JpcHQuc3JjID0gJy9kaXN0L2Jvb3QubWluLmpzJztcbiAgICAgICRzY3JpcHQuc3JjID0gJ2h0dHBzOi8vY2RuLnNtYXJ0dmVsLmNvbS9zY3JpcHRzL2Jvb3QubWluLmpzJztcbiAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoJHNjcmlwdCk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkNoYW5nZXMoKSB7XG4gICAgaWYoIXRoaXMubUVsKXtcbiAgICAgIHRoaXMubUVsID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LmdldEVsZW1lbnRzQnlUYWdOYW1lKCdTbWFydHZlbENvbXBvbmVudCcpWzBdXG4gICAgfVxuICAgIHRoaXMuc2V0UHJvcHModGhpcy5wcm9wcylcbiAgICBpZihcbiAgICAgICEhdGhpcy53aW4ubmF0aXZlV2luZG93LlNNVFdpZGdldFxuICAgICl7XG4gICAgICB0aGlzLndpbi5uYXRpdmVXaW5kb3cuU01UV2lkZ2V0LnVwZGF0ZURhdGFzZXQodGhpcy5wcm9wcyk7XG4gICAgICB2YXIgZXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQoJ1NNVEVuZ2FnZScpO1xuICAgICAgZG9jdW1lbnQuZGlzcGF0Y2hFdmVudChldmVudCk7XG4gICAgfVxuICB9XG5cbiAgbmdPbkRlc3Ryb3koKSB7XG4gICAgdGhpcy53aW4ubmF0aXZlV2luZG93LlNtdEJvb3QuaW5pdGlhbGl6ZWQgPSBmYWxzZVxuICB9XG5cbiAgcHJpdmF0ZSBpbml0aWFsaXplV2lkZ2V0KCl7XG4gICAgaWYoXG4gICAgICAhIXRoaXMud2luLm5hdGl2ZVdpbmRvdy5TbXRCb290ICYmXG4gICAgICAhdGhpcy53aW4ubmF0aXZlV2luZG93LlNtdEJvb3QuaW5pdGlhbGl6ZWRcbiAgICApe1xuICAgICAgdGhpcy53aW4ubmF0aXZlV2luZG93LlNtdEJvb3QuYm9vdCgpO1xuICAgIH1cbiAgfVxuXG4gIHByaXZhdGUgc2V0UHJvcHMocDp7fSl7XG4gICAgY29uc3Qgc2VsZiA9IHRoaXNcbiAgICBpZiAoISFwKXtcbiAgICAgIHZhciBrcyA9IE9iamVjdC5rZXlzKHApXG4gICAgICBpZighIXRoaXMubUVsLmRhdGFzZXQpe1xuICAgICAgICBrcy5mb3JFYWNoKChlbCkgPT4ge1xuICAgICAgICAgIHNlbGYubUVsLmRhdGFzZXRbZWxdID0gcFtlbF1cbiAgICAgICAgfSlcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdfQ==