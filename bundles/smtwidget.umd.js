(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core')) :
    typeof define === 'function' && define.amd ? define('smtwidget', ['exports', '@angular/core'], factory) :
    (global = global || self, factory(global.smtwidget = {}, global.ng.core));
}(this, function (exports, core) { 'use strict';

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    /**
     * @return {?}
     */
    function _window() {
        // return the global native browser window object
        return window;
    }
    var WindowRef = /** @class */ (function () {
        function WindowRef() {
        }
        Object.defineProperty(WindowRef.prototype, "nativeWindow", {
            get: /**
             * @return {?}
             */
            function () {
                return _window();
            },
            enumerable: true,
            configurable: true
        });
        WindowRef.decorators = [
            { type: core.Injectable }
        ];
        return WindowRef;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SmtWidgetComponent = /** @class */ (function () {
        function SmtWidgetComponent(el, win) {
            this.el = el;
            this.win = win;
            this.initializeWidget = this.initializeWidget.bind(this);
            this.setProps = this.setProps.bind(this);
        }
        /**
         * @return {?}
         */
        SmtWidgetComponent.prototype.ngOnInit = /**
         * @return {?}
         */
        function () {
            if (!this.mEl) {
                this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
            }
            document.addEventListener('widgetLoaded', this.initializeWidget, true);
            if (!this.win.nativeWindow.SmtBoot) {
                /** @type {?} */
                var $script = document.createElement('script');
                //$script.src = '/dist/boot.min.js';
                $script.src = 'https://cdn.smartvel.com/scripts/boot.min.js';
                document.body.appendChild($script);
            }
        };
        /**
         * @return {?}
         */
        SmtWidgetComponent.prototype.ngOnChanges = /**
         * @return {?}
         */
        function () {
            if (!this.mEl) {
                this.mEl = this.el.nativeElement.getElementsByTagName('SmartvelComponent')[0];
            }
            this.setProps(this.props);
            if (!!this.win.nativeWindow.SMTWidget) {
                this.win.nativeWindow.SMTWidget.updateDataset(this.props);
                /** @type {?} */
                var event = new CustomEvent('SMTEngage');
                document.dispatchEvent(event);
            }
        };
        /**
         * @return {?}
         */
        SmtWidgetComponent.prototype.ngOnDestroy = /**
         * @return {?}
         */
        function () {
            this.win.nativeWindow.SmtBoot.initialized = false;
        };
        /**
         * @private
         * @return {?}
         */
        SmtWidgetComponent.prototype.initializeWidget = /**
         * @private
         * @return {?}
         */
        function () {
            if (!!this.win.nativeWindow.SmtBoot &&
                !this.win.nativeWindow.SmtBoot.initialized) {
                this.win.nativeWindow.SmtBoot.boot();
            }
        };
        /**
         * @private
         * @param {?} p
         * @return {?}
         */
        SmtWidgetComponent.prototype.setProps = /**
         * @private
         * @param {?} p
         * @return {?}
         */
        function (p) {
            /** @type {?} */
            var self = this;
            if (!!p) {
                /** @type {?} */
                var ks = Object.keys(p);
                if (!!this.mEl.dataset) {
                    ks.forEach((/**
                     * @param {?} el
                     * @return {?}
                     */
                    function (el) {
                        self.mEl.dataset[el] = p[el];
                    }));
                }
            }
        };
        SmtWidgetComponent.decorators = [
            { type: core.Component, args: [{
                        selector: 'ngx-smtwidget',
                        template: '<SmartvelComponent></SmartvelComponent>',
                        providers: [WindowRef]
                    }] }
        ];
        /** @nocollapse */
        SmtWidgetComponent.ctorParameters = function () { return [
            { type: core.ElementRef },
            { type: WindowRef }
        ]; };
        SmtWidgetComponent.propDecorators = {
            props: [{ type: core.Input }]
        };
        return SmtWidgetComponent;
    }());

    /**
     * @fileoverview added by tsickle
     * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
     */
    var SmtWidgetModule = /** @class */ (function () {
        function SmtWidgetModule() {
        }
        SmtWidgetModule.decorators = [
            { type: core.NgModule, args: [{
                        declarations: [SmtWidgetComponent],
                        imports: [],
                        exports: [SmtWidgetComponent],
                        schemas: [core.CUSTOM_ELEMENTS_SCHEMA, core.NO_ERRORS_SCHEMA],
                    },] }
        ];
        return SmtWidgetModule;
    }());

    exports.SmtWidgetComponent = SmtWidgetComponent;
    exports.SmtWidgetModule = SmtWidgetModule;
    exports.WindowRef = WindowRef;

    Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=smtwidget.umd.js.map
